import React from 'react'

type ConditionProps = {
    activeId: string | number,
    children: JSX.Element[] | JSX.Element
}

const Condition: React.FC<ConditionProps> = ({activeId, children}) => {
    const requiredChildrenProp = 'id'
    const isArrayChildren = Array.isArray(children)
    const isObjectChildren = children instanceof Object && !isArrayChildren

    const renderArrayChild = () => {
        const arrayOfChildren = children as JSX.Element[]

        return arrayOfChildren.map((child) => {
            const isArrayChild = Array.isArray(child)
            const isObjectChild = child instanceof Object && !isArrayChild
            if (isObjectChild) {
                return checkChildToRequiredProp(child)
            }
            return null
        })
    }

    const checkChildToRequiredProp = (childElement: JSX.Element) => {
        const elementProps = childElement.props

        if (elementProps?.hasOwnProperty(requiredChildrenProp)) {
            /// if childElement has 'id' prop
            const isEqualRequiredProps = activeId === elementProps[requiredChildrenProp]
            if (isEqualRequiredProps) {
                /// if activeId === id of childElement
                return childElement
            } else {
                return null
            }

        } else {
            throw new Error('When using a condition component, you must specify the "id" property for all its child components.')
        }
    }

    if (children) {
        if (isArrayChildren) {
            /// If child is an Array
            return (
                <>
                    { renderArrayChild() }
                </>
            )
        }
        if (isObjectChildren) {
            /// If a child component is only one and it's an Array
            return checkChildToRequiredProp(children as JSX.Element)
        }
    }

    return null
}

export default Condition