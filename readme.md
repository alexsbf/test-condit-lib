# Conditional-lib ( React + TS )

Библиотека предназначена для рендера компонентов в зависимости от условия.


## Установка библиотеки

`npm install https://alexsbf@bitbucket.org/alexsbf/condition-render-lib.git`

## Использование

Импортируем компонент **Conditional** из пакета **conditional-lib**

`import Conditional from "conditional-lib"`  

Данный компонент принимает **обязательное свойство activeId** 

Тип activeId - **number** или **string**

```
<Condition activeId="111">
    //children Components
</Condition>
```
Внутрь компонента передаем дочерние компоненты, которые **обязаны** иметь свойство **id**.
Если свойство **activeId** компонента **Conditional** и свойство **id** дочернего компонента равны между собой (===), 
то данный дочерний компонент будет отображен.

#### Пример кода

```
function App() {
    const todos = ['Купить хлеба', 'Купить масла', 'Погладить кота']

    return (
        <Condition activeId="111">
            <Block 
                todos={todos}
                id="111"
            />

            <Block 
                todos={todos}
                id="id15"
            />

            <Block 
                todos={todos}
                id="111"
            />

            <Block 
                todos={todos}
                id="0cw"
            />
        </Condition>
    )
}

export default App
```

В данном примере будут выведены на экран только два компонента с **id="111"**
